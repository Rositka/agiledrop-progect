<?php

namespace Drupal\eventsdate\Services;

class EventsDateShow {

  public function daysBeforeEventStarts($date): string
  {
    $days = $this->getDifferenceInDays($date);

    if ($days >= 1) {
      return $days . ' days left before the start of the event';
    }

    if ($days === 0) {
      return 'This event happens today';
    }

    return 'This event has already passed';
  }

  public function getDifferenceInDays($date): float
  {
    $now = time();
    $event_date = strtotime($date);
    $differenceDays = $event_date - $now;

    return round($differenceDays / (60 * 60 * 24));
  }
}
