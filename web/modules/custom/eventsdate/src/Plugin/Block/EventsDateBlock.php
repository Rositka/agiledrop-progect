<?php

namespace Drupal\eventsdate\Plugin\Block;

use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\eventsdate\Services\EventsDateShow;
use Psr\Container\ContainerInterface;

/**
 *
 * @Block (
 *   id = "events_date_block",
 *   admin_label = @Translation("Events Date Block"),
 *   category = @Translation("Events Date Block"),
 * )
 */
class EventsDateBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   *
   * @var EventsDateShow
   */
  protected EventsDateShow $calcEventsDate;

  /**
   * Constructs an EventsDateBlock object.
   *
   * @param array $configuration
   * The block configuration.
   * @param string $plugin_id
   * The ID of the plugin.
   * @param mixed $plugin_definition
   * The plugin definition.
   * @param EventsDateShow $calcEventsDate
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EventsDateShow $calcEventsDate) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->calcEventsDate = $calcEventsDate;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): EventsDateBlock
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('eventsdate.date_before_start')
    );
  }

  public function build(): array
  {
    $node = \Drupal::routeMatch()->getParameter('node');
    $nid = $node->id();

    $query = \Drupal::database()->select('node__field_date', 'n');
    $query->addField('n', 'field_date_value');
    $query->condition('n.entity_id', $nid);
    $query->range(0, 1);
    $date = $query->execute()->fetchField();

    $output = $this->calcEventsDate->daysBeforeEventStarts($date);

    return [
      '#markup' => $output,
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }
}
